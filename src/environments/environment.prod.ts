export const environment = {
  production: true,
  leerung_api: "https://api.euv-stadtbetrieb.de/leerung/",
  leerung_ical_base: "api.euv-stadtbetrieb.de/leerung-ical/",
  //leerung_ical: "https://api.euv-stadtbetrieb.de/leerung-ical/",
  leerung_pdf: "https://api.euv-stadtbetrieb.de/leerung-pdf/",
  assetsPrefix:"/fileadmin/leerung-web/",
  version: "$VERSION"
};
