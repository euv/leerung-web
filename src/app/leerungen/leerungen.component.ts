import { LeerungDate } from './../model/leerung-date';
import { LeerungenSearchService } from './../services/leerungen-search.service';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Hausnummer } from '../model/hausnummer';
import { Strasse } from '../model/strasse';
import * as moment from 'moment';

interface Monat {
  id: number,
  jahr: number,
  monat: number,
  bezeichnung: string
}

@Component({
  selector: 'app-leerungen',
  templateUrl: './leerungen.component.html',
  styleUrls: ['./leerungen.component.scss']
})
export class LeerungenComponent implements OnChanges {

  @Input() hausnummer!: Hausnummer;
  @Input() strasse!: Strasse;

  selectedDatum: Monat;
  datumauswahl: Monat[] = [];

  monatsnamen: string[] = [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember"
  ];

  leerungen?: LeerungDate;

  constructor(private leerungenSearchService: LeerungenSearchService) {
    let n = moment().date(1).hour(0).minute(0).second(0).millisecond(0);
    console.log("Moment:", n.toString());
    for(let i=0; i<3; ++i) {
      this.datumauswahl.push({
        id: i,
        jahr: n.year(),
        monat: n.month(),
        bezeichnung:  n.year().toFixed(0) +" - "+ this.monatsnamen[n.month()]
      });
      n.add(1, 'months');
    }
    this.selectedDatum = this.datumauswahl[0];
  }

  ngOnChanges(changes: SimpleChanges): void {
      this.loadData();
  }

  loadData(): void {
    this.leerungenSearchService.loadLeerungen(this.hausnummer.id).toPromise().then( (data) => {
      this.leerungen = {
        haId: this.hausnummer.id,
        nummer: this.hausnummer.nummer,
        stId: this.strasse.id,
        strasse: this.strasse.name,
        grau: [],
        gelb: [],
        blau: [],
        braun: [],
      }

      if(data) {
        for(const t of data.grau) {
          const m = moment(t);
          this.leerungen?.grau.push(m);
        }

        for(const t of data.gelb) {
          const m = moment(t);
          this.leerungen.gelb.push(m);
        }

        for(const t of data.braun) {
          const m = moment(t);
          this.leerungen.braun.push(m);
        }

        for(const t of data.blau) {
          const m = moment(t);
          this.leerungen.blau.push(m);
        }
      }
    });
  }

}
