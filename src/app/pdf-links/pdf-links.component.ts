import { Component, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Hausnummer } from '../model/hausnummer';

@Component({
  selector: 'app-pdf-links',
  templateUrl: './pdf-links.component.html',
  styleUrls: ['./pdf-links.component.scss']
})
export class PdfLinksComponent {

  @Input() hausnummer!: Hausnummer;

  constructor() { }

  /* Display if min 1. Dec of this year */
  displayNextYear(): boolean {
    let now = new Date();
    let at = new Date(new Date().getFullYear(), 11, 1);
    return now >= at;
  }

  getLinkCurrentYear(): string {
    return environment.leerung_pdf +"v1/"+ this.hausnummer.id +"?jahr="+ this.getCurrentYear();
  }

  getLinkNextYear(): string {
    return environment.leerung_pdf +"v1/"+ this.hausnummer.id +"?jahr="+ this.getNextYear();
  }

  getCurrentYear(): string {
    let year = new Date().getFullYear();
    return year.toString();
  }

  getNextYear(): string {
    let year = new Date().getFullYear();
    return (year+1).toString();
  }

  openPdfCurrentYear(): void {
    const url = this.getLinkCurrentYear();
    window.open(url, "_blank");
  }

  openPdfNextYear(): void {
    const url = this.getLinkNextYear();
    window.open(url, "_blank");
  }
}
