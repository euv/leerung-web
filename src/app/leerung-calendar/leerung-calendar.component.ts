import { environment } from 'src/environments/environment';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { LeerungDate } from '../model/leerung-date';
import * as moment from 'moment';

interface DayInfo {
  display: string,
  inMonth: boolean,
  grau?: boolean,
  gelb?: boolean,
  blau?: boolean,
  braun?: boolean,
}

@Component({
  selector: 'app-leerung-calendar',
  templateUrl: './leerung-calendar.component.html',
  styleUrls: ['./leerung-calendar.component.scss']
})
export class LeerungCalendarComponent implements OnChanges {

  @Input() leerungen?: LeerungDate;
  @Input() jahr?: number;
  @Input() monat?: number;

  drawArray?: DayInfo[][];
  environment: any;

  constructor() {
    this.environment = environment;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.draw();
  }

  getStartMonday(d: moment.Moment): moment.Moment {
    return d.weekday(1);
  }

  draw(): void {
    if(this.jahr && this.monat!=undefined && this.leerungen) {
      let drawDate = this.getStartMonday(moment().year(this.jahr).month(this.monat).startOf("month"));
      if(drawDate.isAfter(moment().year(this.jahr).month(this.monat).startOf("month"))) {
        drawDate = drawDate.add(-1, "week");
      }
      const lastDate = moment(moment().year(this.jahr).month(this.monat).startOf("month")).add(1, 'months');

      console.log("Moment:", moment().year(this.jahr).month(this.monat).startOf("month"));
      console.log("DrawDate:", drawDate);
      console.log("LastDate:", lastDate);

      this.drawArray = [];

      while(drawDate.isBefore(lastDate)) {
        let week: DayInfo[] = [];
        for(const day of ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]) {
          //console.log("DS:", drawDate);
          week.push({
            display: drawDate.date().toFixed(0).toString().padStart(2, "0"),
            inMonth: drawDate.month() == this.monat,
            grau: this.leerungen?.grau.findIndex(v => drawDate.isSame(v, 'day'))>=0,
            blau: this.leerungen?.blau.findIndex(v => drawDate.isSame(v, 'day'))>=0,
            braun: this.leerungen?.braun.findIndex(v => drawDate.isSame(v, 'day'))>=0,
            gelb: this.leerungen?.gelb.findIndex(v => drawDate.isSame(v, 'day'))>=0,
          });
          drawDate.add(1, "days");
        }
        this.drawArray?.push(week);
      }
    }
  }
}
