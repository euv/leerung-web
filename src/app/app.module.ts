import { LeerungenSearchService } from './services/leerungen-search.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { ClipboardModule } from '@angular/cdk/clipboard'

import { AutoCompleteModule } from 'primeng/autocomplete';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';

import { AppComponent } from './app.component';
import { StrassenauswahlComponent } from './strassenauswahl/strassenauswahl.component';
import { StrassenSearchService } from './services/strassen-search.service';
import { HausnummernAuswahlComponent } from './hausnummern-auswahl/hausnummern-auswahl.component';
import { HausnummernSearchService } from './services/hausnummern-search.service';
import { LeerungenComponent } from './leerungen/leerungen.component';
import { PdfLinksComponent } from './pdf-links/pdf-links.component';
import { IcalLinksComponent } from './ical-links/ical-links.component';
import { LeerungCalendarComponent } from './leerung-calendar/leerung-calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    StrassenauswahlComponent,
    HausnummernAuswahlComponent,
    LeerungenComponent,
    PdfLinksComponent,
    IcalLinksComponent,
    LeerungCalendarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    AutoCompleteModule,
    DropdownModule,
    ButtonModule,
    ClipboardModule
  ],
  providers: [
    StrassenSearchService,
    HausnummernSearchService,
    LeerungenSearchService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
