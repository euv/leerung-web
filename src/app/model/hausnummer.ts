export interface Hausnummer {
  id: number,
  stId?: number,
  strasse?: string,
  nummer: string
}
