import * as moment from 'moment';

export interface LeerungDate {
  haId?: number,
  stId?: number,
  strasse?: string,
  nummer?: string,
  gelb: moment.Moment[],
  braun: moment.Moment[],
  grau: moment.Moment[],
  blau: moment.Moment[],
}
