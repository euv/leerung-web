import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { Hausnummer } from '../model/hausnummer';

import { Clipboard } from '@angular/cdk/clipboard';

interface Zeit {
  name: string,
  rel: string
}

@Component({
  selector: 'app-ical-links',
  templateUrl: './ical-links.component.html',
  styleUrls: ['./ical-links.component.scss']
})
export class IcalLinksComponent {

  @Input() hausnummer!: Hausnummer;

  environment: any;

  zeiten: Zeit[] = [
    {name: "23 Uhr", rel: "alarm=1"},
    {name: "22 Uhr", rel: "alarm=2"},
    {name: "21 Uhr", rel: "alarm=3"},
    {name: "20 Uhr", rel: "alarm=4"},
    {name: "19 Uhr", rel: "alarm=5"},
    {name: "18 Uhr", rel: "alarm=6"},
    {name: "17 Uhr", rel: "alarm=7"},
    {name: "16 Uhr", rel: "alarm=8"},
    {name: "15 Uhr", rel: "alarm=9"},
    {name: "14 Uhr", rel: "alarm=10"},
    {name: "13 Uhr", rel: "alarm=11"},
    {name: "12 Uhr", rel: "alarm=12"},
    {name: "11 Uhr", rel: "alarm=13"},
    {name: "10 Uhr", rel: "alarm=14"},
    {name: "9 Uhr", rel: "alarm=15"},
    {name: "8 Uhr", rel: "alarm=16"},
    {name: "7 Uhr", rel: "alarm=17"},
    {name: "6 Uhr", rel: "alarm=18"},
    {name: "5 Uhr", rel: "alarm=19"},
    {name: "4 Uhr", rel: "alarm=20"},
    {name: "3 Uhr", rel: "alarm=21"},
    {name: "2 Uhr", rel: "alarm=22"},
    {name: "1 Uhr", rel: "alarm=23"},
    {name: "keine", rel: ""},
  ];

  zeit!: Zeit;

  constructor(private sanitizer: DomSanitizer, private clipboard: Clipboard) {
    this.environment = environment;
    this.zeit = this.zeiten[5];
  }

  sanitize(url: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  /* Display if min 1. Dec of this year */
  displayNextYear(): boolean {
    let now = new Date();
    let at = new Date(new Date().getFullYear(), 11, 1);
    return now >= at;
  }

  getWebcalLink(secure: boolean=false): string {
    let url = "";
    if(secure) {
      url += "webcals://";
    } else {
      url += "webcal://";
    }
    url += environment.leerung_ical_base;
    url += "v1/"+ this.hausnummer.id;
    return url;
  }

  getLink(secure: boolean=true): string {
    let url = "";
    if(secure) {
      url += "https://";
    } else {
      url += "http://";
    }

    url += environment.leerung_ical_base;
    url += "v1/"+ this.hausnummer.id;

    return url;
  }

  getCurrentYear(): string {
    let year = new Date().getFullYear();
    return year.toString();
  }

  getNextYear(): string {
    let year = new Date().getFullYear();
    return (year+1).toString();
  }

  openApple(z: Zeit): void {
    const url = this.getWebcalLink() +"?link=true&"+ z.rel;
    window.open(url, "_blank");
  }

  openGoogle(z: Zeit): void {
    const url = "https://www.google.com/calendar/render?cid="+ encodeURIComponent(this.getWebcalLink() +"?link=true");
    console.debug("Google URL:", url);
    window.open(url, "_blank");
  }

  copyGoogleToClipboard(z: Zeit): void {
    const url = this.getLink() +"?link=true";
    this.clipboard.copy(url);
  }

  openGoogleCalendar() : void {
    window.open("https://calendar.google.com", "_blank");
  }

  openWebcal(z: Zeit): void {
    const url = this.getWebcalLink() +"?link=true&"+ z.rel;
    window.open(url, "_blank");
  }

  copyToClipboard(z: Zeit): void {
    const url = this.getWebcalLink() +"?link=true&"+ z.rel;
    this.clipboard.copy(url);
  }

  copyDownloadLinkToClipboard(z: Zeit): void {
    const url = this.getLink() +"?"+ z.rel;
    this.clipboard.copy(url);
  }

  openDownload(z: Zeit): void {
    const url = this.getLink() +"?"+ z.rel;
    window.open(url, "_blank");
  }
}
