import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Hausnummer } from '../model/hausnummer';
import { HausnummernSearchService } from '../services/hausnummern-search.service';

@Component({
  selector: 'app-hausnummern-auswahl',
  templateUrl: './hausnummern-auswahl.component.html',
  styleUrls: ['./hausnummern-auswahl.component.scss']
})
export class HausnummernAuswahlComponent implements OnInit, OnChanges {

  @Input() stId?: number;
  @Input() disabled: boolean = false;
  @Input() hausnummer!: Hausnummer|null;
  @Output() hausnummerChanged: EventEmitter<Hausnummer> = new EventEmitter<Hausnummer>();

  hausnummern: Hausnummer[] = [];
  selectedHausnummer!: number;

  constructor(private hausnummernSearchService: HausnummernSearchService) { }

  ngOnInit(): void {
    if(this.hausnummer) {
      this.selectedHausnummer = this.hausnummer.id;
      this.hausnummerChanged.emit(this.hausnummer);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["stId"]) {
      const current = changes["stId"].currentValue;
      const previous = changes["stId"].previousValue;

      this.stId = current;
      if(current != previous) {
        this.refresh();
      }
    }
  }

  refresh(): void {
    if(this.stId) {
      this.hausnummernSearchService.searchHausnummern(this.stId).toPromise().then( (data) => {
        if(data) {
          this.hausnummern = data;
          if(!this.hausnummern.find(e => e.id==this.selectedHausnummer)) {
            this.hausnummerChanged.emit(this.hausnummern[0]);
          }
        }
      });
    } else {
      this.hausnummern = [];
    }
  }

  isDisabled(): boolean {
    return this.disabled;
  }

  onChange(event: any): void {
    const haus = this.hausnummern.find(e => e.id==this.selectedHausnummer);
    this.hausnummerChanged.emit(haus);
  }
}
