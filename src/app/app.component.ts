import { environment } from './../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Hausnummer } from './model/hausnummer';
import { Strasse } from './model/strasse';
import { LocalStorageService } from './services/local-storage.service';

@Component({
  selector: 'app-leerung-web',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Leerungsabfrage';

  selectedStrasse!: Strasse|null;
  selectedHausnummer!: Hausnummer|null;

  environment: any;

  constructor(private localStorageService: LocalStorageService) {
    this.environment = environment;
  }

  ngOnInit(): void {
    this.selectedStrasse = this.localStorageService.loadStrasse();
    this.selectedHausnummer = this.localStorageService.loadHausnummer();
  }

  onStrasseChanged(strasse: Strasse): void {
    console.log("Set selected Strasse:", strasse);
    this.selectedStrasse = strasse;
    this.localStorageService.saveStrasse(strasse);
  }

  clearStrasse(): void {
    console.log("Clear selected Strasse");
  }

  onHausnummerChanged(hausnummer: Hausnummer): void {
    console.log("Set selected Hausnummer:", hausnummer);
    this.selectedHausnummer = hausnummer;
    this.localStorageService.saveHausnummer(hausnummer);
  }

  showInfo(): boolean {
    return false;
  }

  debug(event: any): void {
    console.log("DEBUG Event:", event);
  }
}
