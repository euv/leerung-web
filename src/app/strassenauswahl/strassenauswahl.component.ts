import { EventEmitter, Component, OnInit, Output, Input } from '@angular/core';
import { Strasse } from '../model/strasse';
import { StrassenSearchService } from '../services/strassen-search.service';

@Component({
  selector: 'app-strassenauswahl',
  templateUrl: './strassenauswahl.component.html',
  styleUrls: ['./strassenauswahl.component.scss']
})
export class StrassenauswahlComponent implements OnInit {

  @Input() strasse!: Strasse|null;

  @Output() onSelect = new EventEmitter<Strasse>();
  @Output() onClear = new EventEmitter();

  text: string = "";
  results: Strasse[] = [];
  selectedStrasse!: Strasse;

  constructor(private strassenSearchService: StrassenSearchService) { }

  ngOnInit(): void {
    if(this.strasse) {
      this.selectedStrasse = this.strasse;
      this.select(this.selectedStrasse);
    }
  }

  search(event :any): void {
    this.strassenSearchService.searchStrassen(event.query).toPromise().then( (data: Strasse[] | undefined) => {
      this.results = [];
      if(data) {
        for(let s of data) {
          this.results.push(s);
        }
      }
    });
  }

  select(e: Strasse): void {
    this.onSelect.emit(e);
  }

  clear(): void {
    this.onClear.emit();
  }
}
