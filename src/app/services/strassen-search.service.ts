import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Strasse } from '../model/strasse';

@Injectable({
  providedIn: 'root'
})
export class StrassenSearchService {

  constructor(private http: HttpClient) { }

  searchStrassen(search: string): Observable<Strasse[]> {
    return this.http.get<Strasse[]>(environment.leerung_api +"v3/strasse?size=10&search="+search);
  }

  loadStrasse(stId: number): Observable<Strasse> {
    return this.http.get<Strasse>(environment.leerung_api +"v3/strasse/"+ stId);
  }
}
