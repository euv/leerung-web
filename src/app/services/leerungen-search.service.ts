import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LeerungDate } from './../model/leerung-date';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LeerungenSearchService {

  constructor(private http: HttpClient) { }

  loadLeerungen(haId: number, full?: boolean): Observable<LeerungDate> {
    if(full) {
      return this.http.get<LeerungDate>(environment.leerung_api+"v3/leerung/"+ haId +"?full=true");
    } else {
      return this.http.get<LeerungDate>(environment.leerung_api+"v3/leerung/"+ haId);
    }
  }
}
