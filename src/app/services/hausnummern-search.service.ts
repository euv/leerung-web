import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Hausnummer } from '../model/hausnummer';

@Injectable({
  providedIn: 'root'
})
export class HausnummernSearchService {

  constructor(private http: HttpClient) { }

  searchHausnummern(stId: number, full?: boolean): Observable<Hausnummer[]> {
    if(full) {
      return this.http.get<Hausnummer[]>(environment.leerung_api+"v3/haus/?full=true&stId="+ stId);
    }
    return this.http.get<Hausnummer[]>(environment.leerung_api+"v3/haus/?stId="+ stId);
  }
}
