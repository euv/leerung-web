import { Injectable } from '@angular/core';
import { Hausnummer } from '../model/hausnummer';
import { Strasse } from '../model/strasse';

function getLocalStorage() {
  return localStorage;
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private localStorage: Storage;

  constructor() {
    this.localStorage = getLocalStorage();
  }

  saveStrasse(strasse: Strasse): void {
    const jsonData = JSON.stringify(strasse);
    this.localStorage.setItem('strasse', jsonData);
  }

  saveHausnummer(hausnummer: Hausnummer): void {
    const jsonData = JSON.stringify(hausnummer);
    this.localStorage.setItem('hausnummer', jsonData);
  }

  loadStrasse(): Strasse|null {
    const jsonData = this.localStorage.getItem('strasse');
    if(jsonData) {
      return JSON.parse(jsonData);
    }
    return null;
  }

  loadHausnummer(): Hausnummer|null {
    const jsonData = this.localStorage.getItem('hausnummer');
    if(jsonData) {
      return JSON.parse(jsonData);
    }
    return null;
  }
}
