# LeerungWeb

The webfrontend for wast the waste collection service of Castrop-Rauxel.

The project use the Angular framework and the resulting application is embedded
into the website of EUV Stadtbetrieb Castrop-Rauxel.

Contacts and questions to: marcus.schneider@euv-stadtbetrieb.de

# Links
The latest stable release could be visit here:

https://www.euv-stadtbetrieb.de/private-haushalte/leerungsabfrage2/

# License
The project is developed under the GPL3 license.

# API
The project use the open API from EUV, located at:

https://api.euv-stadtbetrieb.de/leerung/swagger-ui.html
